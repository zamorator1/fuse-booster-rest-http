package com.soaint.fuse.services;

import com.soaint.fuse.entities.Persona;

public interface PersonasService {
	
	/**
	 * busco una persona por su nombre
	 * @param nombre
	 * @return el objeto persona
	 */
	Persona buscarPersonaPorNombre(String nombre);

}
