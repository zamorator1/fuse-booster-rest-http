package com.soaint.fuse.services.impl;

import org.apache.camel.Header;
import org.springframework.stereotype.Service;

import com.redhat.fuse.boosters.rest.http.Greetings;
import com.redhat.fuse.boosters.rest.http.GreetingsService;

@Service("greetingsService")

public class GreetingsServiceImpl implements GreetingsService {

    private static final String THE_GREETINGS = "Hello, ";

    @Override
    public Greetings getGreetings(@Header("name") String name ) {
        return new Greetings( THE_GREETINGS + name );
    }

}