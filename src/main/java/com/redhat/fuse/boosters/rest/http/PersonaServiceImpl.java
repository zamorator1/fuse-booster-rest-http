package com.redhat.fuse.boosters.rest.http;

import org.springframework.stereotype.Service;

import com.soaint.fuse.entities.Persona;
import com.soaint.fuse.services.PersonasService;

@Service("personaService")
public class PersonaServiceImpl implements PersonasService {

	@Override
	public Persona buscarPersonaPorNombre(String nombre) {
		Persona p = new Persona();
		p.setNombres(nombre);
		return p;
	}

}
