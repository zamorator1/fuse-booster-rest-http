package com.redhat.fuse.boosters.rest.http;

import org.springframework.context.annotation.Bean;

/**
 * Service interface for name service.
 * 
 */
public interface GreetingsService {

    /**
     * Generate Greetings
     *
     * @return a string greetings
     */
	@Bean
    Greetings getGreetings( String name);

}